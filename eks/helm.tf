provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

#resource "helm_release" "awesome-release" {
#  name       = "awesome-release"
#  chart      = "./awesome"
#
#  values = [
#    file("${path.module}/dev-values.yaml")
#  ]
#}
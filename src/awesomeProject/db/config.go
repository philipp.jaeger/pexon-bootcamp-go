package db

import (
	"database/sql"
	"github.com/go-sql-driver/mysql"
	"os"
)

var sqlConfig = mysql.Config{
	User:                 os.Getenv("DB_USER"),
	Passwd:               os.Getenv("DB_PASSWORD"),
	Net:                  "tcp",
	Addr:                 os.Getenv("DB_HOST") + ":3306",
	DBName:               "books",
	AllowNativePasswords: true,
}

var db, _ = sql.Open("mysql", sqlConfig.FormatDSN())

package db

import (
	. "awesomeProject/data"
	"awesomeProject/helper"
)

func BookStore(book Book) {
	_, err := db.Exec("INSERT INTO books (id, author, title, year_written) VALUES (?, ?, ?, ?)", book.Id, book.Author, book.Title, book.Year)
	helper.HandleError(err)
}

func BookDelete(id int) {
	_, err := db.Exec("DELETE FROM books WHERE id=?", id)
	helper.HandleError(err)
}

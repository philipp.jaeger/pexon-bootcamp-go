package db

import (
	. "awesomeProject/data"
	"awesomeProject/helper"
	"database/sql"
)

func BookSelect(query string) []Book {
	var books []Book
	rows, err := db.Query(query)
	helper.HandleError(err)
	defer func(rows *sql.Rows) {
		err := rows.Close()
		helper.HandleError(err)
	}(rows)
	for rows.Next() {
		var b Book
		err := rows.Scan(&b.Id, &b.Author, &b.Title, &b.Year)
		helper.HandleError(err)
		books = append(books, b)
	}
	return books
}

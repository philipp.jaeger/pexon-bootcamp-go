package main

import (
	"fmt"
	"os"

	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	ginprometheus "github.com/zsais/go-gin-prometheus"

	. "awesomeProject/api"
)

func main() {
	fmt.Println(os.Getenv("DB_USER"))
	fmt.Println(os.Getenv("DB_PASSWORD"))
	fmt.Println(os.Getenv("DB_HOST") + ":3306")

	router := gin.Default()

	pprof.Register(router)

	prometheus := ginprometheus.NewPrometheus("gin")
	RegisterCustomMetrics()
	prometheus.Use(router)

	//TODO remove testing endpoint
	router.GET("/", GetBanner)

	router.GET("/api/v1/book", GetAllBooks)
	router.GET("/api/v1/book/:id", GetBookById)
	router.GET("/api/v1/book/year/:year", GetBooksByYear)

	router.POST("api/v1/book", AddNewBook)
	router.DELETE("/api/v1/book/:id", DeleteBook)

	err := router.Run("0.0.0.0:8080")
	if err != nil {
		return
	}
}

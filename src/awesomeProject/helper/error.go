package helper

import "fmt"

func HandleError(err any) {
	if err != nil {
		fmt.Println(err)
	}
}

// This is the service package for the API.
// In this file, the functions being called by the endpoints are defined.

package api

import (
	. "awesomeProject/data"
	"awesomeProject/db"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetBanner(c *gin.Context) {
	countHttpRequest(c)
	c.JSON(http.StatusOK, "Welcome to the book application!")
}

// Returns a list of all books
func GetAllBooks(c *gin.Context) {
	countHttpRequest(c)
	books := db.BookSelect("SELECT * FROM books")
	c.IndentedJSON(http.StatusOK, books)
}

// Returns a book by its ID, checking if the ID is really unique
func GetBookById(c *gin.Context) {
	countHttpRequest(c)
	id, _ := strconv.Atoi(c.Param("id"))
	books := db.BookSelect("SELECT * FROM books WHERE id=" + strconv.Itoa(id))
	if len(books) == 1 {
		c.IndentedJSON(http.StatusOK, books[0])
	} else {
		// Book not present, or ID not unique
		c.Status(http.StatusNotFound)
	}
}

// Returns a list of all books written in a given year
func GetBooksByYear(c *gin.Context) {
	countHttpRequest(c)
	year, _ := strconv.Atoi(c.Param("year"))
	books := db.BookSelect("SELECT * FROM books WHERE year_written=" + strconv.Itoa(year))
	c.IndentedJSON(http.StatusOK, books)
}

// Inserts a new book, throws an error if it already exists
func AddNewBook(c *gin.Context) {
	countHttpRequest(c)
	var book Book
	err := c.BindJSON(&book)
	if err != nil {
		fmt.Println(err)
	}
	db.BookStore(book)
	c.Status(http.StatusAccepted)
}

// Deletes a book by its id
func DeleteBook(c *gin.Context) {
	countHttpRequest(c)
	id, _ := strconv.Atoi(c.Param("id"))
	db.BookDelete(id)
	c.Status(http.StatusAccepted)
}

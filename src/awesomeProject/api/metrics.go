package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
)

var HttpRequestCounter = prometheus.NewCounter(prometheus.CounterOpts{
	Name: "http_request_counter_total",
	Help: "Number of total http requests",
})

var HttpRequestCounterGet = prometheus.NewCounter(prometheus.CounterOpts{
	Name: "http_request_counter_get",
	Help: "Number of total http GET requests",
})

var HttpRequestCounterPost = prometheus.NewCounter(prometheus.CounterOpts{
	Name: "http_request_counter_post",
	Help: "Number of total http POST requests",
})

var HttpRequestCounterDelete = prometheus.NewCounter(prometheus.CounterOpts{
	Name: "http_request_counter_delete",
	Help: "Number of total http DELETE requests",
})

func RegisterCustomMetrics() {
	prometheus.MustRegister(HttpRequestCounter)
	prometheus.MustRegister(HttpRequestCounterGet)
	prometheus.MustRegister(HttpRequestCounterPost)
	prometheus.MustRegister(HttpRequestCounterDelete)
}

func countHttpRequest(c *gin.Context) {
	fmt.Println(c.Request.Method)
	HttpRequestCounter.Inc()
	switch c.Request.Method {
	case "GET":
		HttpRequestCounterGet.Inc()
	case "POST":
		HttpRequestCounterPost.Inc()
	case "DELETE":
		HttpRequestCounterDelete.Inc()
	}
}

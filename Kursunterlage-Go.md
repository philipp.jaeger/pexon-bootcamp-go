# Kursunterlagen REST API in Go


Das vorliegende Dokument ist ein erster Wurf zum Erstellen eines Kurses zur Implementierung einer REST API in Go, die dann in die jeweiligen Bootcamp-Kurse integriert werden kann.


## Go Grundlagen - Erstellen eines Moduls

Golang/Go ist eine interpretierte Sprache, die zunächst von google entwickelt wurde und inzwischen eine relativ große Community hat. Unter MacOS erfolgt die Installation über homebrew mit dem Befehl
```bash
brew install golang
```

Danach sollte die Binärdatei `go` auf der Konsole ausführbar sein. Sie enthält Paketmanager, Interpreter, Linter, Debugger und Test Runner. 

Zunächst muss ein neues Go-Modul angelegt werden, in dem wir die Anwendung entwickeln.
```bash
mkdir newProject
cd newProject
go mod init newProject
```

Nun können wir mit dem Entwickeln der Anwendung beginnen.

## Abhängigkeiten in Go

Externe Packagee werden in Go über eine Referenz auf das Repository und der unten gezeigten Syntax importiert. Dafür gib es verschiedene Möglichkeiten:
```go
import (
	"fmt"
	"net/http"
	"github.com/gin-gonic/gin"
)
```
Das Package `fmt` ist in der Standardbibliothek enthalten und benötigt daher keinen zusätzlichen Namespace. Dieser ist jedoch z.B. bei `net/http` notwendig. Die dritte Möglichkeit ist der direkte Import eines (öffentlich lesbaren) Git-Repositories, wie bei `github.com/gin-gonic/gin`.

### Import-Modifier

Zusätzlich zum oben gezeigten Standard-Import gibt es die Import-Modifier `.`und `_`, die das Verhalten des Imports verändern. Die Zeile
```go
. Packagee
```
etwa führt dazu, dass alle Member der Packages `Packagee` transparent importiert werden, d.h. auf sie kann anstatt mit `Packagee.Member` einfach mit `Member` zugegriffen werden. Punkt-Imports sollten generell vermieden werden.
```go
_ Packagee
``` 
führt dazu, dass das Package anonym geladen wird. Initialisierungsroutinen werden ausgeführt, aber die Member sind nicht sichtbar (Sideeffect-Import). 

### Eigene Packagee definieren

Als Package bezeichnet man alle go files in einem Ordner. Wenn der Ordner `db` heißt, sollten alle Dateien, die Teil des Packages sind, mit der Zeile
```go
package db
```
beginnen. In diesen Dateien definierte Funktionen etc. können dann mit der Import-Deklaration
```go
"db"
```
verwendet werden. 

### Beispiel


```go
// main.go
package newProject

import (
    "db"
)

func main() {
    var hello = "hello world"
    db.SomeFunction(hello)
}


// db/function.go
package db

import (
    "fmt"
)

func SomeFunction(str string) {
    fmt.Println(str)
}
```

Ausführen des Programms:
```bash
go run .
```

Ausgabe:
```bash
> hello world
```

## Implementierung der REST-Schnittstelle

Es gibt mehrere Bibliotheken, die das Implementieren von REST-APIs erleichtern. In diesem Beispiel verwenden wie die Biblithek `gin-gonic`. Zunächst importieren wir die Bibliothek und initialisieren ein Router-Objekt.

```go
import (
    "github.com/gin-gonic/gin"
)

func main() {
    router := gin.Default()

    err := router.Run("0.0.0.0:8080")
	if err != nil {
		return
	}
}
```
Mit dem obenstehenden Programm wird ein Webserver mit Listen Address 0.0.0.0 auf Port 8080 gestartet.

### Der erste Endpunkt

Als nächstes fügen wir dem Router einen GET-Endpunkt hinzu. Dazu legen wir eine Funktion an, die die Logik enthält, und registrieren diese im Router. Die Funktion könnte wie Folgt aussehen:
```go
func GetHelloWorld(c *gin.Context) {
	countHttpRequest(c)
	c.JSON(http.StatusOK, "Welcome to the application!")
}
```
Hier wurde zusätzlich das Modul `http` verwendet, eines der Standardmodule von Golang. Nun erweitern wir noch die `main`-Funktion um die Zeile
```go
router.GET("/", GetHelloWorld)
```
**Achtung:** Endpunkte müssen nach der Initialisierung des Routers und vor dem Ausführen des Servers registriert werden. 

Analog zu `router.GET` existieren weitere Methoden für die anderen HTTP-Verben.

Navigieren wir nun im Browser auf `http://localhost:8080/`, so erscheint die Ausgabe
```
Welcome to the application!
```

## Anbindung der Datenbank

Die MySQL-Datenbank kann mit dem folgenden Code-Fragment angebunden werden:
```go
package db

import (
	"database/sql"
	"github.com/go-sql-driver/mysql"
	"os"
)

var sqlConfig = mysql.Config{
	User:                 os.Getenv("DB_USER"),
	Passwd:               os.Getenv("DB_PASSWORD"),
	Net:                  "tcp",
	Addr:                 os.Getenv("DB_HOST") + ":3306",
	DBName:               "database",
	AllowNativePasswords: true,
}

var db, _ = sql.Open("mysql", sqlConfig.FormatDSN())
```

Abfragen gegen die Datenbank können mit den Funktionen 
* `db.Query` für `SELECT...` und
* `db.Exec`für `INSERT...` und `DELETE...`

ausgeführt werden. Ein `SELECT...` Query könnte z.B. wie Folgt aussehen:
```go
func select(query) {
    rows, err := db.Query(query)
	helper.HandleError(err)
	defer func(rows *sql.Rows) {
		err := rows.Close()
	}(rows)
	for rows.Next() {
		var b Book
		err := rows.Scan(&b.Id, &b.Author, &b.Title, &b.Year)
		books = append(books, b)
	}
	return books
}
```

## Zusammenfassung

Der Artikel gibt einen Einblick in die Grundlagen der Programmiersprache Golang. Es werden mehrere Bibliotheken und Built-In-Module vorgestellt, mit denen eine REST-Schnittstelle implementiert werden kann. Außerdem wird die Anbindung einer SQL-Datenbank demonstriert.
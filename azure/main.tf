terraform {
  backend "azurerm" {
    subscription_id = "a4bb5e75-5f58-4915-ba81-a6e9297a7194"
    resource_group_name = "test-pj"
    storage_account_name = "tfstatebootcamppj"
    container_name = "containerbootcamppj"
    key = "bookservice.tfstate"
  }
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
}

provider "azurerm" {
  features {
    key_vault {
      purge_soft_delete_on_destroy = true
    }
  }
}

resource "azurerm_resource_group" "bootcamp" {
  name     = "test-pj"
  location = "East US"
}

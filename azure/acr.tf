resource "azurerm_container_registry" "registry" {
  location            = "East US"
  name                = "bootcampphilippjaegerregistry"
  resource_group_name = "test-pj"
  sku = "Standard"
}

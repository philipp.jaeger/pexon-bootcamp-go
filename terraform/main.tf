terraform {
  required_providers {
    aws = {
        source = "hashicorp/aws"
        version = "~> 4.0"
    }
  }
  backend "s3" {
    bucket = "cloudbootcamp-philipp-jaeger-bucket"
    key = "main.tfstate"
    region = "eu-central-1"
  }
}

provider "aws" {
  region = "eu-central-1"
  profile = "SandboxAccess-524233844543"
}

resource "aws_ecr_repository" "registry" {
  name                 = "cloudbootcamp-philipp-jaeger-containerregistry"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
} 

resource "aws_secretsmanager_secret" "db-user" {
  name = "db-user"
}

resource "aws_secretsmanager_secret" "db-password" {
  name = "db-password"
}

resource "aws_secretsmanager_secret" "db-host" {
  name = "db-host"
}

data "aws_secretsmanager_secret_version" "db-user" {
  secret_id = aws_secretsmanager_secret.db-user.id
}

data "aws_secretsmanager_secret_version" "db-password" {
  secret_id = aws_secretsmanager_secret.db-password.id
}

data "aws_secretsmanager_secret_version" "db-user" {
  secret_id = aws_secretsmanager_secret.db-user.id
}

output "name" {
  value = data.aws_secretsmanager_secret_version.db-user.secret_string
  sensitive = false
}
